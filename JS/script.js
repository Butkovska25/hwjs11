    let passwordField = document.querySelector("#password");
    let checkPasswordField = document.querySelector("#confirm-password");
    let enterBtn = document.querySelector(".btn");
    let formError = document.querySelector('.text-alert');
    let form = document.querySelector('.password-form');

    form.addEventListener('click', (e) => {
      e.preventDefault()
      if (e.target.tagName === 'I') {
        e.target.classList.toggle('fa-eye-slash');
        let inputId = e.target.previousElementSibling
        if (inputId.type === "password") {
          inputId.type = 'text'
        } else {
          inputId.type = 'password'
        }
      }
    })
    enterBtn.addEventListener('click', (e) => {
      e.preventDefault()
      if (passwordField.value === checkPasswordField.value) {
        document.getElementById ('message').innerHTML = ""
        alert('You are welcome')

      } else {
        document.getElementById ('message').innerHTML = "Потрібно ввести однакові значення"
        formError.style.color = 'red'
      }
    })